window.onerror = function(msg, url, line, col, error) {
	if (error.code != null) {
		if (error.code === 3064) 
		{
			swal({
			  title: 'Whoops!',
			  text: "Your session is not valid. You need to relogin. The page is gonna refresh.",
			  type: 'error',
			  showCancelButton: false,
			  allowOutsideClick: false,
			  allowEscapeKey: false,
			  // confirmButtonColor: '#3085d6',
			  // cancelButtonColor: '#d33',
			  confirmButtonText: 'OK',
			  closeOnConfirm: false
			},
			function(isConfirm) {
				console.log(isConfirm);
			  if (isConfirm) {
			   	if (localStorage.Backendless != null) {
			    	delete localStorage.Backendless;
			    	location.href = location.href;
			    }
			  }
			});
		}
	}
}