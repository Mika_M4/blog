'use strict';
var editor;
$(function () {
	$(".button-collapse").sideNav();
	var APPLICATION_ID = '6615B743-43E4-BF29-FF68-331815AF1500',
		SECRET_KEY = '3583D783-F9D1-F00E-FF4C-C33930225A00',
		VERSION = 'v1';
	Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);

	try {
		if (Backendless.UserService.isValidLogin()) {
			userLoggedIn(Backendless.LocalCache.get('current-user-id'));
		}
		else {
			var loginScript = $('#login-template').html();
			var loginTemplate = Handlebars.compile(loginScript);
			$('.main-container').html(loginTemplate);
		}
	}
	catch (ex) {
		Backendless.UserService.logout();
		if (localStorage.Backendless !== null) {
			delete localStorage.Backendless;
		} 
		location.href = location.href;
	}
	$(document).on('click', '.cancel-new-blog', function() {
		$('.main-container').html(welcomeHTML);
	})
	$(document).on('submit', '.form-signin', function(event) {
		event.preventDefault();

		var data = $(this).serializeArray(),
			email = data[0].value,
			password = data[1].value;
		try {
			Backendless.UserService.login(email, password, true, new Backendless.Async(userLoggedIn, gotError));
		}
		catch (ex) {
			ShowAlert(ex);
		}
	});

	$(document).on('click', '.add-blog', function() {
		var blogScript = $('#add-blog-template').html();
		var addBlogTemplate = Handlebars.compile(blogScript);

		$('.main-container').html(addBlogTemplate);
		InitEditor();
	});
	$(document).on('submit', '.form-add-blog', function(event) {
		event.preventDefault();

		var data = $(this).serializeArray(),
			title = data[0].value,
			content = data[1].value;

			var dataStore = Backendless.Persistence.of(Posts);
			var postObject = new Posts({
				title: title,
				content: content,
				authorEmail: Backendless.UserService.getCurrentUser().email
			});

			dataStore.save(postObject);
			this.title.value = "";
			editor.setData("");
			Materialize.toast('Post added!', 3000);
	});
	$(document).on('click', '.logout', function () {
		Backendless.UserService.logout(new Backendless.Async(userLoggedOut, gotError));
		var loginScript = $('#login-template').html();
		var loginTemplate = Handlebars.compile(loginScript);

		$('.main-container').html(loginTemplate);
	})
});
function InitEditor() {
	editor = CKEDITOR.replace($('#content')[0]);
}
function Posts(args) {
	args = args || {};
	this.title = args.title || "";
	this.content = args.content || "";
	this.authorEmail = args.authorEmail || "";
}
function userLoggedOut() {

}

var welcomeHTML;
function userLoggedIn(user) {
	console.log('logged in!');
	var userData = user;
	if (typeof user == "string") {
		userData = Backendless.Data.of(Backendless.User).findById(user); 
	}
	var welcomeScript = $('#welcome-template').html();
	var welcomeTemplate = Handlebars.compile(welcomeScript);
	welcomeHTML = welcomeTemplate(userData);
	$('.main-container').html(welcomeHTML);
}
function gotError(error) {
	ShowAlert(error.message);
}

function ShowAlert(message) {
	var message = $('.errors-section').html(
		'<div style="display:none;" class="errors card red lighten-2 white-text">' + 
		' <div class="card-content">' + 
		  message + 
		' </div>' + 
		'</div>'
	).children().first();
	message.slideDown('fast');
	setTimeout(function() {
		message.slideUp('slow', function() {
			message.remove();
		});
	}.bind(this), 2000);
}