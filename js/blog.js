$(function () {
	$(".button-collapse").sideNav();
	var APPLICATION_ID = '6615B743-43E4-BF29-FF68-331815AF1500',
		SECRET_KEY = '3583D783-F9D1-F00E-FF4C-C33930225A00',
		VERSION = 'v1';
	Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
	var postsCollection = Backendless.Persistence.of(Posts).find();
	var admin = AmIAdmin();
	var data = {
		admin: admin,
		posts: postsCollection.data
	};
	Handlebars.registerHelper('format', function(time) {
		return moment(time).format("dddd, MMMM Do YYYY")
	});
	Handlebars.registerHelper('format2', function(time) {
		return moment(time).format("MM/DD/YY")
	});
	var blogScript = $('#blogs-template').html();
	var blogTemplate = Handlebars.compile(blogScript);
	var blogHTML = blogTemplate(data);

	$('.main-container').html(blogHTML);	

	var newPosts = Backendless.Persistence.of(Posts).find({condition: "created >= '" + moment().format("MM/DD/YYYY").toString() + '\''}).data.length;
	if (newPosts > 0) {
		$('.today-posts').html(newPosts);
	} else {
		$('.today-posts').hide();
	}
});
function Posts(args) {
	args = args || {};
	this.title = args.title || "";
	this.content = args.content || "";
	this.authorEmail = args.authorEmail || "";
	this.objectId = args.objectId || "";
}
function AmIAdmin() {
	try {
		if (Backendless.UserService.isValidLogin()) 
			return true;
	}
	catch (ex) {
		return false;
	}
	return false;
}
$(document).on('click', '.delete-post', function() {
	if (!AmIAdmin()) {
		return false;
 	}
	$(this).parents().eq(1).slideUp();
	Materialize.toast('Post removed!', 3000);
	var posts = Backendless.Persistence.of(Posts);
	posts.remove($(this).data('id'));

});